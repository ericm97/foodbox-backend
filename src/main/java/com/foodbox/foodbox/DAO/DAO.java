package com.foodbox.foodbox.DAO;

public interface DAO<T> {
    public T getOneById(int id);
}
